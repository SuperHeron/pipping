# Copyright 2013 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# Anything big-endian will have to override this with "be"
BYTE_ORDER ?= le

PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
DOCDIR ?= $(PREFIX)/share/doc/llpp/

CFLAGS ?= -O2 -g
GLPATH ?= +lablGL

OPT ?= 1

srcdir ?= $(PWD)

ifeq ($(OPT),1)
    OCAML_COMPILER = ocamlopt.opt
    SUFFIX         = cmx
    LIB_SUFFIX     = cmxa
    custom         =
else
    OCAML_COMPILER = ocamlc
    SUFFIX         = cmo
    LIB_SUFFIX     = cma
    custom         = -custom
endif

# gnu-make 4.0 syntax
FREETYPE_CFLAGS != freetype-config --cflags
OCAML_COMPILE_FLAGS = \
    -D_GNU_SOURCE \
    $(FREETYPE_CFLAGS) -include ft2build.h \
    $(CFLAGS)

FREETYPE_LIBS != freetype-config --libs
OCAML_LINK_FLAGS = \
    -lmupdf -lmujs \
    -lz -ljpeg -ljbig2dec -lcrypto -lopenjp2 -lpthread \
    -lX11 \
    $(FREETYPE_LIBS) \
    $(LDFLAGS)

LIBRARIES = $(addsuffix .$(LIB_SUFFIX),lablgl str unix)

all: llpp

SUBMODULES = $(BYTE_ORDER)/bo help utils wsi parser
MODULES    = $(SUBMODULES) config main
ML_OBJECTS = $(addsuffix .$(SUFFIX),$(MODULES))
config.$(SUFFIX): $(srcdir)/config.ml $(addsuffix .$(SUFFIX),$(SUBMODULES))
	$(OCAML_COMPILER) -c -o $@ -I $(GLPATH) $<
help.ml:
	$(SHELL) $(srcdir)/mkhelp.sh $(srcdir)/keystoml.ml $(srcdir)/KEYS > help.ml
help.$(SUFFIX): help.ml
	$(OCAML_COMPILER) -c -o $@ $<
$(BYTE_ORDER)/bo.$(SUFFIX): $(BYTE_ORDER)/bo.ml
	$(OCAML_COMPILER) -c -o $@ $<
utils.$(SUFFIX): $(srcdir)/utils.ml
	$(OCAML_COMPILER) -c -o $@ $<
parser.$(SUFFIX): $(srcdir)/parser.ml
	$(OCAML_COMPILER) -c -o $@ $<
wsi.cmi: $(srcdir)/wsi.mli
	$(OCAML_COMPILER) -c -o $@ $<
wsi.$(SUFFIX): $(srcdir)/wsi.ml wsi.cmi utils.$(SUFFIX) $(BYTE_ORDER)/bo.$(SUFFIX)
	$(OCAML_COMPILER) -c -I $(BYTE_ORDER) -o $@ $<
si.$(SUFFIX): $(srcdir)/wsi.ml wsi.cmi utils.$(SUFFIX)
	(OCAML_COMPILER) -c -o $@ $<
main.$(SUFFIX): $(srcdir)/main.ml utils.$(SUFFIX) config.$(SUFFIX)
	$(OCAML_COMPILER) -pp "sed -f pp.sed" -c -o $@ -I $(GLPATH) $<

C_OBJECTS  = link.o
link.o: $(srcdir)/link.c
	$(OCAML_COMPILER) -c -o $@ -ccopt "$(OCAML_COMPILE_FLAGS)" $<

llpp: $(ML_OBJECTS) $(C_OBJECTS)
	$(OCAML_COMPILER) $(custom)		\
	    -o $@				\
	    -I $(GLPATH)			\
	    $(LIBRARIES)			\
	    $(C_OBJECTS)			\
	    -cclib "$(OCAML_LINK_FLAGS)"	\
	    $(ML_OBJECTS)

install: llpp
	install -d $(DESTDIR)$(BINDIR)
	install llpp $(DESTDIR)$(BINDIR)
	install -d $(DESTDIR)$(DOCDIR)
	install KEYS README Thanks $(DESTDIR)$(DOCDIR)

clean:
	rm -f \
	    help.ml link.o \
	    $(addsuffix .cmi,$(MODULES)) \
	    $(addsuffix .cmo,$(MODULES)) \
	    $(addsuffix .cmx,$(MODULES)) \
	    $(addsuffix   .o,$(MODULES)) \
	    wsi.cmi llpp
